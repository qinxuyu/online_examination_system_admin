var vm = new Vue({
  el: "#app",
  data: {
    curProNo: -1, //当前的专业号
    curCorNo: -1, //当前的课程号

    teacherInfor: {},

    professionList: [], //专业
    courseList: [], //课程

    relevanceList: []
  },
  methods: {
    /**
     * 获取老师所教授的专业和课程
     */
    getTeacherInfor() {
      let that = this;
      $.post(
        getTeacherInforUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.teacherInfor = res.data.teacherInfor;
          that.professionList = res.data.professionList;
          that.courseList = res.data.courseList;
        }
      );
    },

    /**
     * 获取老师所教授的专业和课程
     */
    getRelevanceList() {
      let that = this;
      $.post(
        getProCorUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.relevanceList = res.data.relevanceList;
        }
      );
    },
    /**
     * 删除教师的授课专业和课程
     */
    deleteRelevance() {
      let conf = confirm("删除操作，请谨慎!");
      if (conf == true) {
        let that = this;
        $.post(
          deleteRelevanceUrl,
          {
            relevance_no: event.target.value
          },
          function(res) {
            if (res.data.code === 100) {
              alert("删除成功");
              that.getRelevanceList();
            } else {
              alert("删除失败");
            }
          }
        );
      }
    },

    /**
     * 添加授课的专业和课程
     */
    addRelevance() {
      const profession_no = $("#selectProfession option:selected").val();
      const course_no = $("#selectCourse option:selected").val();

      debugger;
      if (profession_no == "-1" || course_no == "-1") {
        if (profession_no == "-1") {
          alert("请选择专业");
          throw new Error("请选择专业");
        }
        if (course_no == "-1") {
          alert("请选择课程");
          throw new Error("请选择课程");
        }
      }

      let that = this;
      $.post(
        addRelevanceUrl,
        {
          teacher_no: localStorage.getItem("teacher_no"),
          profession_no: profession_no,
          course_no: course_no
        },
        function(res) {
          if (res.data.code === 100) {
            alert("添加成功");
            that.getRelevanceList();
          }
        }
      );
    },

    /**
     * 修改密码
     */
    changePassWord() {
      const oldPassword = $("#oldPassword").val();
      const newPassWord = $("#newPassWord").val();

      if (oldPassword == "" || oldPassword == undefined) {
        alert("请输入原密码");
        throw new Error("请输入原密码");
      }

      if (newPassWord == "" || newPassWord == undefined) {
        alert("请输入新密码");
        throw new Error("请输入新密码");
      }

      $.post(
        changePassWordUrl,
        {
          teacher_no: localStorage.getItem("teacher_no"),
          oldPassword: md5(oldPassword),
          newPassWord: md5(newPassWord)
        },
        function(res) {
          if (res.data.code === 100) {
            alert("密码修改成功");
          } else {
            alert("原密码输入错误");
          }

          $("#oldPassword").val("");
          $("#newPassWord").val("");
        }
      );
    },

    /**
     * x显示修改个人信息
     */
    showTeacherInfor() {
      $("#teacher_no").val(localStorage.getItem("teacher_no"));
      $("#teacher_name").val(this.teacherInfor.teacher_name);
      $("#teacher_phone").val(this.teacherInfor.teacher_phone);
    },

    /**
     * 修改教师个人信息
     */
    changeTeacherInfor() {
      const old_no = localStorage.getItem("teacher_no");
      const teacher_no = $("#teacher_no").val();
      const teacher_name = $("#teacher_name").val();
      const teacher_phone = $("#teacher_phone").val();

      if (teacher_no == "" || teacher_no == undefined) {
        alert("请输入工号");
        throw new Error("请输入工号");
      }
      if (teacher_no.length != 11) {
        alert("请输入11位工号");
        throw new Error("请输入11位工号");
      }
      if (teacher_name == "" || teacher_name == undefined) {
        alert("请输入姓名");
        throw new Error("请输入姓名");
      }
      if (teacher_phone == "" || teacher_phone == undefined) {
        alert("请输入手机号码");
        throw new Error("请输入手机号码");
      }
      if (teacher_phone.length != 11) {
        alert("请输入11位手机号码");
        throw new Error("请输入11位手机号码");
      }

      let that = this;
      $.post(
        changeTeacherInforUrl,
        {
          old_no: localStorage.getItem("teacher_no"),
          teacher_no: teacher_no,
          teacher_name: teacher_name,
          teacher_phone: teacher_phone
        },
        function(res) {
          if (res.data.code === 100) {
            alert("修改成功");
            localStorage.setItem("teacher_no", teacher_no);
            that.getTeacherInfor();
          } else {
            alert("修改失败");
          }
        }
      );
    }
  },

  mounted() {
    if (localStorage.getItem("teacher_no") == "") {
      alert('请登录管理员后台管理');
      window.location.href='index.html';
    }
    this.getTeacherInfor();
    this.getRelevanceList();
  }
});
