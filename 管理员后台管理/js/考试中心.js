var vm = new Vue({
  el: "#app",
  data: {
    curProNo: -1, //当前的专业号
    curCorNo: -1, //当前的课程号
    disabled: true,
    paperList: [], //考试列表
    relevanceList: []
  },
  methods: {
    /**
     * 获取老师所教授的专业和课程
     */
    getProCor() {
      let that = this;
      $.post(
        getProCorUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.relevanceList = res.data.relevanceList;
          that.deletePreOption();
          that.deletePreOption2();
        }
      );
    },

    /**
     * 去掉重复的课程选项
     */
    deletePreOption() {
      $(function() {
        $("#selectId option").each(function() {
          text = $(this).text();
          if ($("#selectId option:contains(" + text + ")").length > 1)
            $("#selectId option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function() {
        $("#selectProfession option").each(function() {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    changeOption() {
      this.curProNo = value = $("#selectProfession option:selected").val();
      this.curCorNo = value = $("#selectCourse option:selected").val();

      if (this.curProNo != -1 && this.curCorNo != -1) {
        this.disabled = false;
        this.getPaperList();
      } else {
        this.paperList = [];
      }
    },

    /**
     * 获取考试列表
     */
    getPaperList() {
      let that = this;
      $.get(allPaperUrl, function(res) {
        that.paperList = res.data.paperList;
      });
    },

    changeStatue() {
      let that = this;
      $.post(
        changeStatueUrl,
        {
          paper_no: event.target.id,
          statue: event.target.value
        },
        function(res) {
          that.getPaperList();
        }
      );
    }
  },
  mounted() {
    if (localStorage.getItem("admin_no") == "") {
      alert("请登录管理员后台管理");
      window.location.href = "index.html";
    }
    this.getPaperList();
  }
});
