var vm = new Vue({
  el: "#app",
  data: {
    adminInfor: ""
  },
  methods: {
    getInfor() {
      let that = this;
      $.post(
        getInforUrl,
        {
          admin_no: localStorage.getItem("admin_no")
        },
        function(res) {
          that.adminInfor = res.data.adminInfor;
        }
      );
    },
    /**
     * 修改密码
     */
    changePassWord() {
      let oldpw = $("#oldpw").val();
      let admin_pw = $("#newpw").val();

      if (oldpw == "" || oldpw == undefined) {
        alert("原密码输入不能为空");
        throw new Error("原密码输入不能为空");
      }

      if (admin_pw == "" || admin_pw == undefined) {
        alert("新密码输入不能为空");
        throw new Error("新密码输入不能为空");
      }

      $.post(
        changePassWordUrl,
        {
          admin_no: localStorage.getItem("admin_no"),
          oldpw: md5(oldpw),
          admin_pw: md5(admin_pw)
        },
        function(res) {
          if (res.data.code === 100) {
            alert("密码修改成功");
            $("#newpw").val("");
          } else {
            alert("原密码错误。");
          }
          $("#oldpw").val("");
          $("#newpw").val("");
        }
      );
    },

    /**
     * 显示待修改个人信息
     */
    showAdminInfor() {
      $("#admin_no").val(this.adminInfor.admin_no);
      $("#admin_name").val(this.adminInfor.admin_name);
      $("#admin_phone").val(this.adminInfor.admin_phone);
    },

    /**
     * 修改个人信息
     */
    changeAdminInfor() {
      const admin_no = $("#admin_no").val();
      const admin_name = $("#admin_name").val();
      const admin_phone = $("#admin_phone").val();

      if (admin_no == "" || admin_no == undefined) {
        alert("请输入工号");
        throw new Error("请输入工号");
      }
      if (admin_no.length != 11) {
        alert("请输入11位工号");
        throw new Error("请输入11位工号");
      }
      if (admin_name == "" || admin_no == undefined) {
        alert("请输入姓名");
        throw new Error("请输入姓名");
      }
      if (admin_phone == "" || admin_phone == undefined) {
        alert("请输入手机号码");
        throw new Error("请输入手机号码");
      }
      if (admin_phone.length != 11) {
        alert("请输入11位手机号码");
        throw new Error("请输入11位手机号码");
      }

      let that = this;
      $.post(
        changeAdminInforUrl,
        {
          old_no: localStorage.getItem("admin_no"),
          admin_no: admin_no,
          admin_name: admin_name,
          admin_phone: admin_phone
        },
        function(res) {
          if (res.data.code === 100) {
            alert("修改成功");
            localStorage.setItem("admin_no", admin_no);
            that.getInfor();
          } else {
            alert("修改失败");
          }
        }
      );
    }
  },
  mounted() {
    if (localStorage.getItem("admin_no") == "") {
      alert("请登录管理员后台管理");
      window.location.href = "index.html";
    }
    this.getInfor();
  }
});
