var vm = new Vue({
  el: "#app",
  data: {
    type: "radio", //试题类型
    disabled: true,
    disabled2: false,

    curType: -1,
    curProNo: -1, //当前的专业号
    curCorNo: -1, //当前的课程号

    questions: [], //试题列表

    readyMoQuestion: "", //待修改的试题
    readyType: "", //待修改的试题的题型
    relevanceList: []
  },
  methods: {
    getQuestion(profession_no, course_no) {
      let that = this;
      $.post(
        questionsUrl,
        {
          profession_no: profession_no,
          course_no: course_no
        },
        function(res) {
          that.questions = res.data.questions;
        }
      );
    },

    /**
     * 添加试题
     */
    addQuestion(questionInfo) {
      let that = this;
      $.post(
        addQuestionUrl,
        {
          questionInfo: JSON.stringify(questionInfo)
        },
        function(res) {
          if (res.data.code === 100) {
            alert("添加成功");
            that.getQuestion(that.curProNo, that.curCorNo);
            $("#question").val("");
            $("#optionA").val("");
            $("#optionB").val("");
            $("#optionC").val("");
            $("#optionD").val("");
          } else {
            alert("添加失败");
          }
        }
      );
    },

    /**
     * 删除试题
     */
    deleteQuestion() {
      let conf = confirm("删除操作，请谨慎!");
      if (conf == true) {
        let that = this;
        $.post(
          deleteQuestionUrl,
          {
            profession_no: this.curProNo,
            course_no: this.curCorNo,
            que_no: event.target.id
          },
          function(res) {
            if (res.data.code === 100) {
              alert("删除成功");
              that.getQuestion(that.curProNo, that.curCorNo);
            } else {
              alert("删除失败");
            }
          }
        );
      }
    },

    /**
     * 渲染需要修改的试题和选项,封装新试题
     */
    showMoQuestion() {
      let que_no = event.target.id;
      for (let i = 0; i < this.questions.length; i++) {
        if (this.questions[i].que_no == que_no) {
          this.readyMoQuestion = this.questions[i];
          if (this.readyMoQuestion.type == "单选题") {
            this.readyType = "radio";
          } else {
            this.readyType = "checkbox";
          }
        }
      }

      $("#question_2").val(this.readyMoQuestion.question);
      if (this.readyMoQuestion.type != "主观题") {
        this.disabled2 = false;
        let option = this.readyMoQuestion.option;
        $("#optionA_2").val(option.A);
        $("#optionB_2").val(option.B);
        $("#optionC_2").val(option.C);
        $("#optionD_2").val(option.D);
      } else {
        this.disabled2 = true;
      }
    },

    /**
     * 修改试题和试题选项
     */
    upDataQuestion(questionInfo) {
      let that = this;
      $.post(
        upDataQuestionUrl,
        {
          questionInfo: JSON.stringify(questionInfo)
        },
        function(res) {
          if (res.data.code === 100) {
            alert("修改成功");
            that.getQuestion(that.curProNo, that.curCorNo);
          } else {
            alert("修改失败");
          }
        }
      );
    },

    /**
     * 获取老师所教授的专业和课程
     */
    getProCor() {
      let that = this;
      $.post(
        getProCorUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.relevanceList = res.data.relevanceList;
          that.deletePreOption();
          that.deletePreOption2();
        }
      );
    },

    /**
     * 去掉重复的课程选项
     */
    deletePreOption() {
      $(function() {
        $("#selectCourse option").each(function() {
          text = $(this).text();
          if ($("#selectCourse option:contains(" + text + ")").length > 1)
            $("#selectCourse option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function() {
        $("#selectProfession option").each(function() {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 修改题目类型
     */
    changeType() {
      this.curType = $("#selectType option:selected").val();

      if (this.curType == 1) {
        this.type = "radio";
        this.disabled = false;
      } else if (this.curType == 2) {
        this.type = "checkbox";
        this.disabled = false;
      } else {
        this.disabled = true;
      }
    },

    changeOption() {
      this.curProNo = value = $("#selectProfession option:selected").val();
      this.curCorNo = value = $("#selectCourse option:selected").val();

      if (this.curProNo != -1 && this.curCorNo != -1) {
        this.getQuestion(this.curProNo, this.curCorNo);
      } else {
        this.questions = [];
      }
    },

    /**
     * 组装要添加的试题和选项
     */
    zuzhuangQuestion() {
      let questionInfo = {
        profession_no: this.curProNo,
        course_no: this.curCorNo,
        question: $("#question").val(),
        type: $("#selectType option:selected").text(),
        goodkey: "",
        options: {
          A: "",
          B: "",
          C: "",
          D: ""
        }
      };

      if (questionInfo.type != "主观题") {
        questionInfo.options = {
          A: $("#optionA").val(),
          B: $("#optionB").val(),
          C: $("#optionC").val(),
          D: $("#optionD").val()
        };
        //   单选题
        if (questionInfo.type == "单选题") {
          questionInfo.goodkey = $("#input input[type='radio']:checked").val();
        }
        // 多选题
        else {
          $("#input input[type='checkbox']:checked").each(function() {
            if (questionInfo.goodkey == "") {
              questionInfo.goodkey = $(this).val();
            } else {
              questionInfo.goodkey += "," + $(this).val();
            }
          });
        }
      }
      return questionInfo;
    },

    /**
     * 组装要修改的试题和选项
     */
    zuzhuangQuestion2() {
      var questionInfo = {
        que_no: this.readyMoQuestion.que_no,
        profession_no: this.curProNo,
        course_no: this.curCorNo,
        question: $("#question_2").val(),
        type: this.readyMoQuestion.type,
        goodkey: ""
      };

      if (this.readyMoQuestion.type != "主观题") {
        questionInfo.options = {
          A: $("#optionA_2").val(),
          B: $("#optionB_2").val(),
          C: $("#optionC_2").val(),
          D: $("#optionD_2").val()
        };

        //   单选题
        if (questionInfo.type == "单选题") {
          questionInfo.goodkey = $(
            "#input_2 input[type='radio']:checked"
          ).val();
        }
        // 多选题
        else {
          $("#input_2 input[type='checkbox']:checked").each(function() {
            if (questionInfo.goodkey == "") {
              questionInfo.goodkey = $(this).val();
            } else {
              questionInfo.goodkey += "," + $(this).val();
            }
          });
        }
      }

      //没有设置正确答案
      if (questionInfo.type != "主观题") {
        if (questionInfo.goodkey == "" || questionInfo.goodkey == undefined) {
          alert("请设置正确选项");
          throw new Error("请设置正确选项");
        }

        if (questionInfo.type == "多选题") {
          if (questionInfo.goodkey.length === 1) {
            alert("多选题请设置多个正确选项");
            throw new Error("多选题请设置多个正确选项");
          }
        }
      }
      return questionInfo;
    },

    /**
     * 提交新试题
     */
    submit() {
      if (this.curType == -1) {
        alert("请选择题目类型");
        throw new Error("请选择题目类型");
      }
      if (this.curProNo == -1) {
        alert("请选择专业");
        throw new Error("请选择专业");
      }

      if (this.curCorNo == -1) {
        alert("请选择课程");
        throw new Error("请选择课程");
      }

      let questionInfo = this.zuzhuangQuestion();

      if (questionInfo.question.length === 0) {
        alert("请输入题目");
        throw new Error("请输入题目");
      }

      if (questionInfo.type != "主观题") {
        if (questionInfo.goodkey == "" || questionInfo.goodkey == undefined) {
          alert("请设置正确答案");
          throw new Error("请设置正确答案");
        }

        let options = questionInfo.options;

        if (
          options.A == "" ||
          options.B == "" ||
          options.C == "" ||
          options.D == ""
        ) {
          alert("请设置完整的选项");
          throw new Error("请设置完整的选项");
        }

        if (questionInfo.type == "多选题") {
          if (questionInfo.goodkey.length == 1) {
            alert("多选题请设置多个正确选项");
            throw new Error("多选题请设置多个正确选项");
          }
        }
      }

      this.addQuestion(questionInfo);
    },

    /**
     * 提交修改的问题和选项
     */
    submit2() {
      let questionInfo = this.zuzhuangQuestion2();
      this.upDataQuestion(questionInfo);
    },
    /**
     * 模糊查询问题
     */
    searchQuestion() {
      let condition = $("#search").val();
      let that = this;
      $.post(
        searchQuestionUrl,
        {
          profession_no: that.curProNo,
          course_no: that.curCorNo,
          condition: condition
        },
        function(res) {
          that.questions = res.data.questions;
        }
      );
    }
  },
  mounted() {
    if (localStorage.getItem("teacher_no") == "") {
      alert("请登录管理员后台管理");
      window.location.href = "index.html";
    }
    this.getProCor();
  }
});
