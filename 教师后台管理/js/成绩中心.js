var vm = new Vue({
  el: "#app",
  data: {
    questions: [],

    specificMarkList: [],
    relevanceList: []
  },
  methods: {
    //获得特定专业特定课程的学生成绩
    getMark(profession_no, course_no) {
      let that = this;
      $.post(
        specificMarkListUrl,
        {
          profession_no: profession_no,
          course_no: course_no
        },
        function(res) {
          that.specificMarkList = res.data.specificMarkList;
        }
      );
    },

    /**
     * 切换单选框
     */
    changeOption() {
      let profession_no = $("#selectProfession option:selected").val();
      let course_no = $("#selectCourse option:selected").val();
      if (course_no != -1 || profession_no != -1) {
        this.getMark(profession_no, course_no);
      } else {
        this.specificMarkList = [];
      }
    },

    /**
     * 获取老师所教授的专业和课程
     */
    getProCor() {
      let that = this;
      $.post(
        getProCorUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.relevanceList = res.data.relevanceList;

          that.deletePreOption();
          that.deletePreOption2();
        }
      );
    },

    /**
     * 去掉重复的课程选项
     */
    deletePreOption() {
      $(function() {
        $("#selectCourse option").each(function() {
          text = $(this).text();
          if ($("#selectCourse option:contains(" + text + ")").length > 1)
            $("#selectCourse option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function() {
        $("#selectProfession option").each(function() {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    }
  },
  mounted() {
    if (localStorage.getItem("teacher_no") == "") {
      alert("请登录管理员后台管理");
      window.location.href = "index.html";
    }
    this.getProCor();
  }
});
