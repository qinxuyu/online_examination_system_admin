const ApiRootUrl = "http://localhost:8360/";

const allPaperUrl = ApiRootUrl + "paper/allPaper"; //获取全部考试接口
const changeStatueUrl = ApiRootUrl + "paper/changeStatue"; //改变考试状态接口

const allProCouListUrl = ApiRootUrl + "admin/allProCou"; //获取全部专业和课程接口
const allMarkListUrl = ApiRootUrl + "admin/allMarkList"; //获取全部成绩
const specificMarkListUrl = ApiRootUrl + "mark/getSpecificMarkList"; //根据专业和课程获得学生成绩

const allStudentInfoUrl = ApiRootUrl + "admin/getStudentInfor"; //获取学生信息接口

const allTeacherInfoUrl = ApiRootUrl + "admin/getTeacherInfor"; //获取教师信息接口
const loginUrl = ApiRootUrl + "login/checkNoAdminPassWord"; //登录接口
const getInforUrl = ApiRootUrl + "admin/getInformation"; //获取管理员信息接口
const changeAdminInforUrl = ApiRootUrl + "admin/changeAdminInfor"; //修改管理员信息接口

const changePassWordUrl = ApiRootUrl + "admin/changePassWord"; //修改管理员密码接口

const addStudentUrl = ApiRootUrl + "admin/addStudent"; //添加学生信息接口
const deleteStudentUrl = ApiRootUrl + "admin/deleteStudent"; //删除学生信息接口
const updateStudentUrl = ApiRootUrl + "admin/updateStudent"; //修改学生信息接口

const addTeacherUrl = ApiRootUrl + "admin/addTeacher"; //添加教师信息接口
const deleteTeacherUrl = ApiRootUrl + "admin/deleteTeacher"; //删除教师信息

const updateTeacherInforUrl = ApiRootUrl + "admin/updateTeacherInfor"; //修改教师信息接口
const updateTeacherPassWordUrl = ApiRootUrl + "admin/updateTeacherPassWord"; //修改教师密码接口