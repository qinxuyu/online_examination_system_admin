const ApiRootUrl = "http://localhost:8360/";

const questionsUrl = ApiRootUrl + "question/getQuestionByProCou"; //获取试题接口
const addQuestionUrl = ApiRootUrl + "question/addQuestion"; //添加试题接口
const deleteQuestionUrl = ApiRootUrl + "question/deleteQuestion"; //删除试题接口
const upDataQuestionUrl = ApiRootUrl + "question/upDataQuestion"; //修改试题接口
const searchQuestionUrl = ApiRootUrl + "question/searchQuestion"; //模糊查询试题接口
const specificMarkListUrl = ApiRootUrl + "mark/getSpecificMarkList"; //根据专业和课程获得学生成绩
const zhuanguanZuoDaUrl = ApiRootUrl + "modifyZhuguan/getZhuguanrRcords"; //获取主观题作答记录接口
const newZhuanguanZuoDaUrl = ApiRootUrl + "modifyZhuguan/upDataZhuguanrRcords"; //提交批改的主观题作答记录接口
const getProCorUrl = ApiRootUrl + "teacher/getProCor"; //获取老师所教授的专业和课程接口
const getPaperListUrl = ApiRootUrl + "paper/getPaperByProCou"; //获取考试列表接口
const addPaperUlr = ApiRootUrl + "paper/addPaper"; //添加考试接口
const loginUrl = ApiRootUrl + "login/chacNoPassWord"; //教师登录接口
const getTeacherInforUrl = ApiRootUrl + "teacher/getTeacherInfor"; //获取教师个人信息

const deleteRelevanceUrl = ApiRootUrl + "teacher/deleteRelevance"; //删除教师的授课专业和课程
const addRelevanceUrl = ApiRootUrl + "teacher/addRelevance"; //添加教师的授课专业和课程

const changePassWordUrl = ApiRootUrl + "teacher/changePassWord"; //修改教师密码接口
const changeTeacherInforUrl = ApiRootUrl + "teacher/changeTeacherInfor"; //修改教师个人信息接口