var vm = new Vue({
	el: "#app",
	data: {
		paperList: [], //考试列表
		relevanceList: []
	},
	methods: {
		submit() {
			const teacher_no = $("#teacher_no").val();
			const teacher_pw = $("#teacher_pw").val();

			if (teacher_no == "" || teacher_no == undefined) {
				alert("请输入工号");
				throw new Error("[请输入工号]");
			}
			if (teacher_pw == "" || teacher_pw == undefined) {
				alert("请输入密码");
				throw new Error("[请输入密码]");
			}

			$.post(
				loginUrl, {
					teacher_no: teacher_no,
					teacher_pw: md5(teacher_pw)
				},
				function(res) {
					if (res.code === 101) {
						alert("无效帐号");
						$("#teacher_no").val("");
						$("#teacher_pw").val("");
						throw new Error("[无效帐号]");
					}

					if (res.code === 102) {
						alert("密码错误");
						$("#teacher_pw").val("");
						throw new Error("[密码错误]");
					}

					localStorage.setItem("teacher_no", $("#teacher_no").val());
					window.location.replace("考试中心.html");
				}
			);
		}
	},
	mounted() {
		localStorage.setItem("teacher_no", "");
	}
});
