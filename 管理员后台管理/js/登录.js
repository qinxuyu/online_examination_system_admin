var vm = new Vue({
  el: "#app",
  data: {},
  methods: {
    submit() {

      let admin_no = $("#admin_no").val();
      let admin_pw = $("#admin_pw").val();

      if (admin_no == "" || admin_no == undefined) {
        alert("请输入工号");
        throw new Error("[请输入工号]");
      }
      if (admin_no.length != 11) {
        alert("请输入11位工号");
        throw new Error("[请输入11位工号]");
      }
      if (admin_pw == "" || admin_pw == undefined) {
        alert("请输入密码");
        throw new Error("[请输入密码]");
      }

      $.post(
        loginUrl, {
          admin_no: admin_no,
          admin_pw: md5(admin_pw)
        },
        function (res) {
          if (res.code === 101) {
            alert("无效帐号");
            $("#admin_no").val("");
            $("#admin_pw").val('');
            throw new Error("[无效帐号]");
          }
          if (res.code === 102) {
            alert("密码错误");
            $("#admin_pw").val('');
            throw new Error("[密码错误]");
          }
          localStorage.setItem("admin_no", $("#admin_no").val());
          window.location.replace("考试中心.html");
        }
      );
    }
  },
  mounted() {
    localStorage.setItem("admin_no", '');
  }
});