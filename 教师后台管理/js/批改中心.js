var vm = new Vue({
  el: "#app",
  data: {
    curProNo: -1, //当前的专业号
    curCorNo: -1, //当前的课程号

    disabled: true,
    predisabled: true,
    nextdisabled: true,

    index: 0,
    record: {}, //当前的答题记录
    recordList: [], //答题记录

    relevanceList: []
  },
  methods: {
    /**
     * 获取答题记录
     */
    getRecordList() {
      let that = this;

      $.post(
        zhuanguanZuoDaUrl,
        {
          profession_no: that.curProNo,
          course_no: that.curCorNo
        },
        function(res) {
          that.recordList = res.data.recordList;
          that.record = that.recordList[0];

          if (that.recordList.length != 0) {
            that.disabled = false;
            that.nextdisabled = false;
            return;
          }
        }
      );
    },

    /**
     * 获取老师所教授的专业和课程
     */
    getProCor() {
      let that = this;
      $.post(
        getProCorUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.relevanceList = res.data.relevanceList;
          console.log(that.relevanceList);
          that.deletePreOption();
          that.deletePreOption2();
        }
      );
    },

    /**
     * 去掉重复的课程选项
     */
    deletePreOption() {
      $(function() {
        $("#selectCourse option").each(function() {
          text = $(this).text();
          if ($("#selectCourse option:contains(" + text + ")").length > 1)
            $("#selectCourse option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function() {
        $("#selectProfession option").each(function() {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    changeOption() {
      this.curProNo = value = $("#selectProfession option:selected").val();
      this.curCorNo = value = $("#selectCourse option:selected").val();

      if (this.curProNo != -1 && this.curCorNo != -1) {
        this.getRecordList();
      } else {
        this.recordList = [];
      }
    },

    /**
     * 切换答题记录
     */
    changeRecord(event) {
      $("#mark").val("");
      this.index = parseInt(event.target.value);
      this.record = this.recordList[this.index];
      this.checkBtn();
    },

    getMark() {
      this.record.mark = $("#mark").val();
      for (let i = 0; i < this.recordList.length; i++) {
        if (this.recordList[i].que_no == this.recordque_no) {
          this.recordList[i] = this.record;
          break;
        }
      }
    },

    /**
     * 检查按钮状态
     */
    checkBtn() {
      if (this.index === 0) {
        this.predisabled = true;
        this.nextdisabled = false;
      }
      if (this.index === this.recordList.length - 1) {
        this.predisabled = false;
        this.nextdisabled = true;
      }
    },

    /**
     * 提交批改
     */
    submit() {
      let that = this;
      //检查没有批改的答题记录
      for (let i = 0; i < this.recordList.length; i++) {
        if (
          this.recordList[i].mark == "" ||
          this.recordList[i].mark == undefined
        ) {
          $("#mark").val("");
          this.record = this.recordList[i];
          this.index = this.recordList.indexOf(this.recordList[i]);
          this.checkBtn();
          alert("出现“漏批”");
          throw new Error("出现“漏批”");
        }
      }
      $.post(
        newZhuanguanZuoDaUrl,
        {
          recordList: JSON.stringify(this.recordList)
        },
        function(res) {
          console.log(res.data);
          if (res.data.code === 100) {
            that.clear();
            alert("批改完成");
          } else {
            alert("提交失败，请重新提交");
          }
        }
      );
    },

    clear() {
      this.index = 0;
      this.record = {};
      this.recordList = [];
      this.disabled = true;
      this.predisabled = true;
      this.nextdisabled = true;
    }
  },
  mounted() {
    if (localStorage.getItem("teacher_no") == "") {
      alert("请登录教师后台管理");
      window.location.href = "index.html";
    }
    this.getProCor();
  }
});
