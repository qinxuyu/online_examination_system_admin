var vm = new Vue({
  el: "#app",
  data: {
    procouList: [], //专业课程数组
    markList: [] //成绩列表
  },
  methods: {
    //获得特定专业特定课程的学生成绩
    fun(profession_no, course_no) {
      let that = this;
      $.post(
        specificMarkListUrl,
        {
          profession_no: profession_no,
          course_no: course_no
        },
        function(res) {
          that.markList = res.data.specificMarkList;
        }
      );
    },

    /**
     * 切换单选框
     */
    changeOption() {
      let profession_no = $("#selectProfession option:selected").val();
      let course_no = $("#selectCourse option:selected").val();
      if (course_no != -1 && profession_no != -1) {
        this.fun(profession_no, course_no);
      } else {
        this.allMark();
      }
    },

    /**
     * 获取所有专业和课程
     */
    allProCor() {
      let that = this;
      $.get(allProCouListUrl, function(res) {
        that.procouList = res.data.procouList;

        that.deletePreOption();
        that.deletePreOption2();
      });
    },

    /**
     * 获取所有学生成绩
     */
    allMark() {
      let that = this;
      $.get(allMarkListUrl, function(res) {
        that.markList = res.data.markList;
      });
    },

    /**
     * 去掉重复的课程选项
     */
    deletePreOption() {
      $(function() {
        $("#selectCourse option").each(function() {
          text = $(this).text();
          if ($("#selectCourse option:contains(" + text + ")").length > 1)
            $("#selectCourse option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function() {
        $("#selectProfession option").each(function() {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    }
  },
  mounted() {
    if (localStorage.getItem("admin_no") == "") {
      alert("请登录管理员后台管理");
      window.location.href = "index.html";
    }
    this.allProCor();
    this.allMark();
  }
});
