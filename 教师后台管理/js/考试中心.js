var vm = new Vue({
  el: "#app",
  data: {
    curProNo: -1, //当前的专业号
    curCorNo: -1, //当前的课程号
    disabled: true,
    paperList: [], //考试列表
    relevanceList: []
  },
  methods: {
    /**
     * 获取老师所教授的专业和课程
     */
    getProCor() {
      let that = this;
      $.post(
        getProCorUrl,
        {
          teacher_no: localStorage.getItem("teacher_no")
        },
        function(res) {
          that.relevanceList = res.data.relevanceList;
          that.deletePreOption();
          that.deletePreOption2();
        }
      );
    },

    /**
     * 去掉重复的课程选项
     */
    deletePreOption() {
      $(function() {
        $("#selectCourse option").each(function() {
          text = $(this).text();
          if ($("#selectCourse option:contains(" + text + ")").length > 1)
            $("#selectCourse option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function() {
        $("#selectProfession option").each(function() {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    changeOption() {
      this.curProNo = value = $("#selectProfession option:selected").val();
      this.curCorNo = value = $("#selectCourse option:selected").val();

      if (this.curProNo != -1 && this.curCorNo != -1) {
        this.disabled = false;
        this.getPaperList();
      } else {
        this.paperList = [];
        this.disabled = true;
      }
    },

    /**
     * 获取考试列表
     */
    getPaperList() {
      let that = this;

      $.post(
        getPaperListUrl,
        {
          profession_no: that.curProNo,
          course_no: that.curCorNo
        },
        function(res) {
          that.paperList = res.data.paperList;
        }
      );
    },

    /**
     * 发布申请
     */
    addPaper() {
      let paper = this.zuzhuangPaper();
      let that = this;
      $.post(
        addPaperUlr,
        {
          paper: JSON.stringify(paper)
        },
        function(res) {
          if (res.data.code === 100) {
            alert("申请成功");
            that.getPaperList();

            $("#num1").val();
            $("#num2").val();
            $("#num3").val();

            $("#mark1").val();
            $("#mark2").val();
            $("#mark3").val();
            $("#mark").val();

            $("#stime").val();
            $("#etime").val();
          } else {
            alert("申请失败");
            throw new Error("申请失败");
          }
        }
      );
    },

    /**
     * 组装考试
     */
    zuzhuangPaper() {
      let paper = {
        profession_no: this.curProNo,
        course_no: this.curCorNo,
        num1: $("#num1").val(),
        mark1: $("#mark1").val(),
        num2: $("#num2").val(),
        mark2: $("#mark2").val(),
        num3: $("#num3").val(),
        mark3: $("#mark3").val(),
        mark: $("#mark").val(),
        stime: this.formatter($("#stime").val()),
        etime: this.formatter($("#etime").val()),
        statue: 1
      };
      return paper;
    },

    /**
     * 计算总分
     */
    compute() {
      let mark1 = $("#num1").val() * $("#mark1").val();
      let mark2 = $("#num2").val() * $("#mark2").val();
      let mark3 = $("#num3").val() * $("#mark3").val();

      $("#mark").val(mark1 + mark2 + mark3);
    },

    submit() {
      if (this.curProNo == -1) {
        alert("请选择专业");
        throw new Error("请选择专业");
      }
      if (this.curCorNo == -1) {
        alert("请选择课程");
        throw new Error("请选择课程");
      }
      if ($("#mark").val() === "" || parseInt($("#mark").val()) === 0) {
        alert("考试总分为0");
        throw new Error("考试总分为0");
      }

      let stime = this.formatter($("#stime").val());
      let etime = this.formatter($("#etime").val());

      if (stime == "") {
        alert("请输入考试开始时间");
        throw new Error("请输入考试开始时间");
      }
      if (etime == "") {
        alert("请输入考试结束时间");
        throw new Error("请输入考试结束时间");
      }

      if (Date.parse(etime) - Date.parse(stime) <= 0) {
        alert("错误的开始（结束）时间");
        throw new Error("错误的开始（结束）时间");
      }

      if (Date.parse(new Date()) - Date.parse(stime) >= 0) {
        alert("错误的开始时间");
        throw new Error("错误的开始时间");
      }

      if (Date.parse(new Date()) - Date.parse(etime) >= 0) {
        alert("错误的结束时间");
        throw new Error("错误的结束时间");
      }

      this.addPaper();
    },

    formatter(time) {
      return time.replace("T", " ") + ":00";
    }
  },
  mounted() {
    if (localStorage.getItem("teacher_no") == "") {
      alert("请登录管理员后台管理");
      window.location.href = "index.html";
    }
    this.getProCor();
  }
});
