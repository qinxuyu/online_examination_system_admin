var vm = new Vue({
  el: "#app",
  data: {
    studentInfoList: [], //学生信息
    readyMoStuInfo: {}, //待修改的学生信息
    teacherInforList: [], //教师信息
    readyMoTeaInfo: {}, //待修改的教师信息
    procouList: [], //专业课程表

    stuFlag: true,
    teaFlag: false
  },
  methods: {
    /**
     * 切换单选框
     */
    changeOption() {
      let value = $("#selectInfor option:selected").val();
      if (value == 1) {
        this.stuFlag = true;
        this.teaFlag = false;
        this.allStudentInfor();
      } else {
        this.allTeacherInfor();
        this.stuFlag = false;
        this.teaFlag = true;
      }
    },

    /**
     * 获取所有专业和课程
     */
    allProCor() {
      let that = this;
      $.get(allProCouListUrl, function (res) {
        that.procouList = res.data.procouList;

        that.deletePreOption();
        that.deletePreOption2();
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption() {
      $(function () {
        $("#selectProfession_add option").each(function () {
          text = $(this).text();
          if (
            $("#selectProfession_add option:contains(" + text + ")").length > 1
          )
            $(
              "#selectProfession_add option:contains(" + text + "):gt(0)"
            ).remove();
        });
      });
    },

    /**
     * 去掉重复的专业选项
     */
    deletePreOption2() {
      $(function () {
        $("#selectProfession option").each(function () {
          text = $(this).text();
          if ($("#selectProfession option:contains(" + text + ")").length > 1)
            $("#selectProfession option:contains(" + text + "):gt(0)").remove();
        });
      });
    },

    /**
     * 获取所有学生信息
     */
    allStudentInfor() {
      let that = this;
      $.get(allStudentInfoUrl, function (res) {
        that.studentInfoList = res.data.studentInfoList;
      });
    },

    /**
     * 获取所有教师信息
     */
    allTeacherInfor() {
      let that = this;
      $.get(allTeacherInfoUrl, function (res) {
        that.teacherInforList = res.data.teacherInforList;
      });
    },

    /**
     * 删除学生信息
     */
    deleteStudent() {
      let conf = confirm("删除操作，请谨慎!");
      if (conf == true) {
        let that = this;
        $.post(
          deleteStudentUrl, {
            student_no: event.target.value
          },
          function (res) {
            if (res.data.code === 100) {
              alert("删除成功");
              that.allStudentInfor();
            } else {
              alert("删除失败");
            }
          }
        );
      }
    },

    /**
     * 显示要修改的学生信息
     */
    showMoStudentInfo() {
      for (let i = 0; i < this.studentInfoList.length; i++) {
        if (this.studentInfoList[i].student_no == event.target.value) {
          this.readyMoStuInfo = this.studentInfoList[i];
          $("#student_no").val(this.readyMoStuInfo.student_no);
          $("#student_name").val(this.readyMoStuInfo.student_name);
          $("#student_phone").val(this.readyMoStuInfo.student_phone);
          break;
        }
      }
      this.allProCou();
      this.deletePreOption2();
    },

    /**
     * 发送修改后的学生信息
     */
    updateStudentInfor() {
      let old_no = event.target.value;
      let student_no = $("#student_no").val();
      let profession_no = $("#selectProfession option:selected").val();
      let student_name = $("#student_name").val();
      let student_phone = $("#student_phone").val();

      //空输入处理;
      if (student_no == "" || student_no == undefined) {
        alert("请输入学号");
        throw new Error("请输入学号");
      }
      if (student_no.length != 11) {
        alert("请输入11位学号");
        throw new Error("请输入11位学号");
      }
      if (profession_no == -1) {
        alert("请选择专业");
        throw new Error("请选择专业");
      }
      if (student_name == "" || student_name == undefined) {
        alert("请输入姓名");
        throw new Error("请输入姓名");
      }

      if (student_phone == "" || student_phone == undefined) {
        alert("请输入手机号码");
        throw new Error("请输入手机号码");
      }

      if (student_phone.length != 11) {
        alert("请输入11位手机号码");
        throw new Error("请输入11位手机号码");
      }

      let that = this;
      $.post(
        updateStudentUrl, {
          old_no: old_no,
          student_no: student_no,
          profession_no: profession_no,
          student_name: student_name,
          student_phone: student_phone
        },
        function (res) {
          if (res.data.code === 100) {
            alert("修改成功");
            that.allStudentInfor();
          } else {
            alert("修改失败");
          }
        }
      );
    },

    /**
     * 添加学生信息
     */
    addStudentInfor() {
      let student_no = $("#student_no_add").val();
      let student_name = $("#student_name_add").val();
      let student_pw = $("#student_pw_add").val();
      let profession_no = $("#selectProfession_add option:selected").val();
      let student_phone = $("#student_phone_add").val();

      if (student_no == "" || student_no == undefined) {
        alert("请输入学号");
        throw new Error("请输入学号");
      }

      if (student_name == "" || student_name == undefined) {
        alert("请输入教师姓名");
        throw new Error("请输入教师姓名");
      }

      if (student_pw == "" || student_pw == undefined) {
        alert("请输入密码");
        throw new Error("请输入密码");
      }

      if (profession_no == -1) {
        alert("请选择专业");
        throw new Error("请选择专业");
      }

      if (student_phone == "" || student_phone == undefined) {
        alert("请输入手机号码");
        throw new Error("请输入手机号码");
      }

      if (student_phone.length != 11) {
        alert("请输入11位手机号码");
        throw new Error("请输入11位手机号码");
      }

      let that = this;
      $.post(
        addStudentUrl, {
          student_no: student_no,
          student_name: student_name,
          profession_no: profession_no,
          student_phone: student_phone,
          student_pw: md5(student_pw)
        },
        function (res) {
          if (res.data.code === 100) {
            alert("添加成功");
            that.allStudentInfor();
          } else {
            alert("该学号已存在。");
          }
          // 清空输入框
          $("#student_no_add").val("");
          $("#student_name_add").val("");
          $("#student_password_add").val("");
          $("#student_phone_add").val("");
        }
      );
    },

    /**
     * 添加教师信息
     */
    addTeacherInfor() {
      let teacher_no = $("#teacher_no_add").val();
      let teacher_name = $("#teacher_name_add").val();
      let teacher_pw = $("#teacher_pw_add").val();
      let teacher_phone = $("#teacher_phone_add").val();

      if (teacher_no == "" || teacher_no == undefined) {
        alert("请输入教师工号");
        throw new Error("请输入教师工号");
      }

      if (teacher_name == "" || teacher_name == undefined) {
        alert("请输入教师姓名");
        throw new Error("请输入教师姓名");
      }

      if (teacher_pw == "" || teacher_pw == undefined) {
        alert("请输入密码");
        throw new Error("请输入密码");
      }

      if (teacher_phone == "" || teacher_phone == undefined) {
        alert("请输入手机号码");
        throw new Error("请输入手机号码");
      }

      if (teacher_phone.length != 11) {
        alert("请输入11位手机号码");
        throw new Error("请输入11位手机号码");
      }

      let that = this;
      $.post(
        addTeacherUrl, {
          teacher_no: teacher_no,
          teacher_name: teacher_name,
          teacher_pw: md5(teacher_pw),
          teacher_phone: teacher_phone
        },
        function (res) {
          if (res.data.code === 100) {
            alert("添加成功");
            that.allTeacherInfor();
          } else {
            alert("该工号已存在。");
          }
          // 清空输入框
          $("#teacher_no_add").val("");
          $("#teacher_name_add").val("");
          $("#teacher_pw_add").val("");
          $("#teacher_phone_add").val("");
        }
      );
    },

    /**
     * 删除教师信息
     */
    deleteTeacherInfor() {
      let conf = confirm("删除操作，请谨慎!");
      if (conf == true) {
        let that = this;
        $.post(
          deleteTeacherUrl, {
            teacher_no: event.target.value
          },
          function (res) {
            if (res.data.code === 100) {
              alert("删除成功");
              that.allTeacherInfor();
            } else {
              alert("删除失败");
            }
          }
        );
      }
    },

    /**
     * 找到待修改教师信息
     */
    getReadyMoTeaInfo() {
      for (let i = 0; i < this.teacherInforList.length; i++) {
        if (this.teacherInforList[i].teacher_no == event.target.value) {
          this.readyMoTeaInfo = this.teacherInforList[i];
          break;
        }
      }
    },

    /**
     * 显示修改教师信息
     */
    showUpdataTeacher() {
      this.getReadyMoTeaInfo();
      $("#teacher_no_updata").val(this.readyMoTeaInfo.teacher_no);
      $("#teacher_name_updata").val(this.readyMoTeaInfo.teacher_name);
      $("#teacher_phone_updata").val(this.readyMoTeaInfo.teacher_phone);
    },

    /**
     * 发送修改的教师信息
     */
    updataTeaerInfor() {
      let teacher_no = $("#teacher_no_updata").val();
      let teacher_name = $("#teacher_name_updata").val();
      let teacher_phone = $("#teacher_phone_updata").val();

      if (teacher_no == "" || teacher_no == undefined) {
        alert("请输入教师工号");
        throw new Error("请输入教师工号");
      }

      if (teacher_name == "" || teacher_name == undefined) {
        alert("请输入教师姓名");
        throw new Error("请输入教师姓名");
      }

      if (teacher_phone == "" || teacher_phone == undefined) {
        alert("请输入手机号码");
        throw new Error("请输入手机号码");
      }

      if (teacher_phone.length != 11) {
        alert("请输入11位手机号码");
        throw new Error("请输入11位手机号码");
      }

      let that = this;
      $.post(
        updateTeacherInforUrl, {
          old_no: event.target.value,
          teacher_no: teacher_no,
          teacher_name: teacher_name,
          teacher_phone: teacher_phone
        },
        function (res) {
          console.log(res.data);

          if (res.data.code === 100) {
            alert("修改成功");
            that.allTeacherInfor();
          } else if (res.data.code === 101) {
            alert("该工号已经存在");
          } else {
            alert("修改失败");
          }
        }
      );
    },

    /**
     * 修改教师密码
     */
    updateTeacherPassWord() {
      this.getReadyMoTeaInfo();
      let teacher_pw = $("#teacher_pw_updata").val();

      if (teacher_pw == "" || teacher_pw == undefined) {
        alert("请输入密码");
        throw new Error("请输入密码");
      }
      $.post(
        updateTeacherPassWordUrl, {
          teacher_no: event.target.value,
          teacher_pw: md5(teacher_pw)
        },
        function (res) {
          if (res.data.code === 100) {
            alert("修改成功");
          } else {
            alert("修改失败");
          }
          $("#teacher_pw_updata").val("");
        }
      );
    }
  },
  mounted() {
    this.allStudentInfor();
    this.allProCor();
  }
});